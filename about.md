---
layout: page
title: "About"
permalink: /about/
---

<style>
	.me
	{
		float: left;
		width: 50%;
		margin-right: 7.5px;
    	margin-bottom: 7.5px;
	}  
</style>

<p><img src="/assets/images/me.jpg/" class="me">I started this website for writing down my thoughts.</p>
<p>
  This blog is a reflection of how I go about my life, picking up random interests and building something interesting out of it.
</p>
<p>
  Primarily those things are about computers and computer programming.
</p>
<p>
  If you wish to talk to me, hit me with a <b>Hi</b> on <a href="mailto:kvipe@mail.ru">kvipe@mail.ru</a>.
</p>