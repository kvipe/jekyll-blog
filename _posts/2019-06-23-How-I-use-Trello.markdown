---
layout: post
title:  "How I use Trello"
date:   2019-06-23 13:00:00 +0300
categories: jekyll update
---

Hello everyone! 

Today it's so boring and useless to just sit around. One should to develop own abilities, expand horizons and always learn new and better ways to be more productive and interesting.

But once you start, you can't stop. It's amazing, isn't it?

Let me write about difficulties which await you. One of them is a mess in your head. You are always planning something to do, trying to remember new information, etc. If you had read David Allen's book "Getting Things Done" you know what is your real enemy. Throughout the book we frequently see advices like "outside your mind " or "avoid thinking". It's true, I went through it.

Trello helps to solve this problem. Though, many of us use it on the Kanban methodology I want to offer another ways of using Trello.
![My Trello using case](/assets/images/trello_using_case.png)

Boards, Lists, Cards are the main entities we use in Trello. Each of them as well as a folder in your File system. Of course, in theory, one could plain text files instead. But, what about sharing? I'm sure you have several devices which are used depending on the situation. It's only browser and Internet access you need to use Trello. Service supports Markdown like formatting language for handy writting texts.

I make it a rule to create a Card for a book which I am reading. There's a chance that I think to switch to another book, but my reading progress, theses, new words (I write them in comments to Card) will remain there. See image below to better understand my idea.
![My Trello using case](/assets/images/trello_books.png)

As well as books you may store your ideas, plans, url links with its detailed description (I bet you often forget what some url in your bookmarks intended for). I follow that.

One last thing I note, I use Trello Lists for fast notes and questions.

Have a think about you Trello use cases. 

Hope this post helps you!

Cheers!