---
layout: post
title:  "The first thoughts"
date:   2019-05-31 13:18:13 +0300
categories: jekyll update
---

Hello everyone! 

I have decided to create this website just for fun (this is reflected in the domain). Frankly, I was suprised that such domain name was available. 

I believe that writting posts helps me to improve my English and maybe turns me to the grafoman. To be fair, I didn't like to write essays in my school time, although I always got good grades for this.

One day I had seen something about Jekyll and became interested in this. Some time latter I ventured to try it out on the practice. It's so handy to post articles just by a git push command. Try to remember what efforts it required in times when git wasn't.

Sometimes I'm getting interesting ideas, so I would be glad to share them with you. Blogging motivates you to be productive and creative.

Like I wrote on the about page, my posts will be on computer themes. If this blog is going to succeed I will install a comment plugin.

Thanks for attention! Cheers.